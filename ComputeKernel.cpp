//
//  ComputeKernel.cpp
//  Molecular-Dynamics
//
//  Created by Sagar Dolas on 13/06/16.
//  Copyright �� 2016 Sagar Dolas. All rights reserved.
//

#include "ComputeKernel.hpp"

# define sqr(x) (x * x)


ComputeKernel::ComputeKernel(const ParticleData<real_t> &mass,ParticleData<real_t> &vel,ParticleData<real_t> &position, ParticleData<real_t> & forcenew,
							ParticleData<real_t> &forceold, ParticleData<int> & celllist, ParticleData<int> & particlelist,const ParticleData<u_int> &bdCell ,const real_t xmin, const real_t xmax,
							const real_t ymin, const real_t ymax, const real_t zmin , const real_t zmax , const real_t rcutoff, const u_int numparticles_,
							const real_t sigma,const real_t epsilon,const real_t timestep) :DomainDecompose(xmin,xmax,ymin,ymax,zmin,zmax,rcutoff,numparticles_), mass(&mass) , velocity(&vel) ,position(&position),forcenew(&forcenew),
																							forceold(&forceold),celllist(&celllist),particlelist(&particlelist),boundaryCells(&bdCell),sigma(sigma),epsilon(epsilon),timestep(timestep)
																							 {
    
                                                                                                
                                                                                                
    // Initialising the domain length
    dom_len.resize(3, 0.0 );
    dom_len[0] = xmax - xmin ;
    dom_len[1] = ymax - ymin ;
    dom_len[2] = zmax - zmin ;
    
    // Initialising the ....domain co-ordinates
    dom_cords.resize(6, 0.0);
    dom_cords[0] = xmin ;
    dom_cords[1] = xmax ;
    dom_cords[2] = ymin ;
    dom_cords[3] = ymax ;
    dom_cords[4] = zmin ;
    dom_cords[5] = zmax ;
    
}

ComputeKernel::~ComputeKernel(){
    

}

// Force Update for every particle
void ComputeKernel::forceUpdate() {
    
    // Loop over array of particle position
    for (u_int p =0 ; p <numparticle_ ; ++p) {
        
        // Find out the index of the particle
        u_int pvind = p * 3 ;
        
        // Temporary buffer for relative vector
        std::vector<real_t> relvec(3,0.0) ;
        
        // Temporary vector for force vector
        std::vector<real_t> forcevector(3,0.0) ;
        
        // Neighbouring particle position
        std::vector<real_t> npos(3,0.0) ;
        
        // Find out the particle position
        std::vector<real_t> pos(3,0.0) ;

        pos[0] = (*position)[pvind] ;
        pos[1] = (*position)[pvind + 1] ;
        pos[2] = (*position)[pvind + 2] ;
        
        // Find the 3D cell index it lies in
        std::vector<u_int> indexcell(3,0) ;
        indexcell[0] = (pos[0] - dom_cords[0]) / celllengthx ;
        indexcell[1] = (pos[1] - dom_cords[2]) / celllengthy ;
        indexcell[2] = (pos[2] - dom_cords[4]) / celllengthz ;
        
        // Reinitialize the particle force to zero
        (*forcenew)[pvind]   = 0.0 ;
        (*forcenew)[pvind+1] = 0.0 ;
        (*forcenew)[pvind+2] = 0.0 ;

        // find global cell index of the current cell
        u_int cellindex = globalCellIndex(indexcell[0],indexcell[1],indexcell[2]) ;
        
        // go to this cell , find out the master particle and traverse to linked list
        for (int nbp_id = (*celllist)[cellindex]; nbp_id!= -1; nbp_id = (*particlelist)[nbp_id]  ) {
            if (nbp_id != static_cast<int>(p)){
                // Find out the index of neighbouring particle  in particle buffer
                u_int nbpindex = nbp_id * 3 ;
                
                // find out the position of Neighbouring particle id
                npos[0] = (*position)[nbpindex] ;
                npos[1] = (*position)[nbpindex+1] ;
                npos[2] = (*position)[nbpindex+2] ;
                
                // Find out the relative vector
                for (u_int i =0; i < 3; ++i) {
                    relvec[i] = pos[i] - npos[i] ;
                }
                
                // Since we know that , this particle lies in close proximity to other particles , we dont have worry about ...minimum distance function
                // now we calculate leonard Jones Potential
				//minDist(relvec,pos,npos,dom_len);
                if (norm(relvec) < rcutoff_){
					leonardJones(relvec, forcevector) ;
                	(*forcenew)[pvind]   += forcevector[0] ;
                	(*forcenew)[pvind+1] += forcevector[1] ;
                	(*forcenew)[pvind+2] += forcevector[2] ;
                }
            }
        }

        // Find the neighbour list of this current cell
        std::vector<u_int> nl(26,0) ;
        findNeighboursPeriodic(nl, indexcell[0], indexcell[1], indexcell[2]) ;

        for (u_int n =0 ; n < nl.size(); ++n) {
            int head_p = (*celllist)[nl[n]] ;
            for (int nbp_id = (head_p);nbp_id != -1;nbp_id = (*particlelist)[nbp_id]) {
            	if (nbp_id != static_cast<int>(p)){
            		// Find out the position of the Neighbouring particle id
					u_int nbpindex = nbp_id * 3 ;

					// Find out the position of the Neighbouring particle id
					npos[0] = (*position)[nbpindex] ;
					npos[1] = (*position)[nbpindex+1] ;
					npos[2] = (*position)[nbpindex+2] ;

					// Find out th relative vector
					for (u_int i =0 ; i < 3; ++i) {
						relvec[i] = pos[i] - npos[i] ;
					}
					// Since we know , that ...now we are looking for particle lie in Neighbourhood cells , we will have to evaluate particle ...minimum distance in order to take care of periodic boundary condition and also check if they ...inside rcutt ..because they may not be inside rcutt .
					minDist(relvec,pos,npos,dom_len);
	                if (norm(relvec) < rcutoff_){
						leonardJones(relvec, forcevector) ;
	                	(*forcenew)[pvind]   += forcevector[0] ;
	                	(*forcenew)[pvind+1] += forcevector[1] ;
	                	(*forcenew)[pvind+2] += forcevector[2] ;
	                }
            	 }
            }
        }
    }
}

void ComputeKernel::forceUpdateReflective(){
    
    
    // Loop over array of particle position
    for (u_int p =0 ; p <numparticle_ ; ++p) {
        
        // Find out the index of the particle
        u_int pvind = p * 3 ;
        
        // Temporary buffer for relative vector
        std::vector<real_t> relvec(3,0.0) ;
        
        // Temporary vector for force vector
        std::vector<real_t> forcevector(3,0.0) ;
        
        // Neighbouring particle position
        std::vector<real_t> npos(3,0.0) ;
        
        // Find out the particle position
        std::vector<real_t> pos(3,0.0) ;
        
        pos[0] = (*position)[pvind] ;
        pos[1] = (*position)[pvind + 1] ;
        pos[2] = (*position)[pvind + 2] ;
        
        // Find the 3D cell index it lies in
        std::vector<u_int> indexcell(3,0) ;
        indexcell[0] = (pos[0] - dom_cords[0]) / celllengthx ;
        indexcell[1] = (pos[1] - dom_cords[2]) / celllengthy ;
        indexcell[2] = (pos[2] - dom_cords[4]) / celllengthz ;
        
        // Reinitialize the particle force to zero
        (*forcenew)[pvind]   = 0.0 ;
        (*forcenew)[pvind+1] = 0.0 ;
        (*forcenew)[pvind+2] = 0.0 ;
        
        // find global cell index of the current cell
        u_int cellindex = globalCellIndex(indexcell[0],indexcell[1],indexcell[2]) ;
        
        // go to this cell , find out the master particle and traverse to linked list
        for (int nbp_id = (*celllist)[cellindex]; nbp_id!= -1; nbp_id = (*particlelist)[nbp_id]  ) {
            if (nbp_id != static_cast<int>(p)){
                // Find out the index of neighbouring particle  in particle buffer
                u_int nbpindex = nbp_id * 3 ;
                
                // find out the position of Neighbouring particle id
                npos[0] = (*position)[nbpindex] ;
                npos[1] = (*position)[nbpindex+1] ;
                npos[2] = (*position)[nbpindex+2] ;
                
                // Find out the relative vector
                for (u_int i =0; i < 3; ++i) {
                    relvec[i] = pos[i] - npos[i] ;
                }
                
                // Since we know that , this particle lies in close proximity to other particles , we dont have worry about ...minimum distance function
                // now we calculate leonard Jones Potential
                //minDist(relvec,pos,npos,dom_len);
                if (norm(relvec) < rcutoff_){
                    leonardJones(relvec, forcevector) ;
                    (*forcenew)[pvind]   += forcevector[0] ;
                    (*forcenew)[pvind+1] += forcevector[1] ;
                    (*forcenew)[pvind+2] += forcevector[2] ;
                }
            }
        }
        
        // Find the neighbour list of this current cell
        std::vector<u_int> nl ;
        findNeighboursReflective(nl, indexcell[0], indexcell[1], indexcell[2]) ;
        
        for (u_int n =0 ; n < nl.size(); ++n) {
            int head_p = (*celllist)[nl[n]] ;
            for (int nbp_id = (head_p);nbp_id != -1;nbp_id = (*particlelist)[nbp_id]) {
                if (nbp_id != static_cast<int>(p)){
                    
                    // Find out the position of the Neighbouring particle id
                    u_int nbpindex = nbp_id * 3 ;
                    
                    // Find out the position of the Neighbouring particle id
                    npos[0] = (*position)[nbpindex] ;
                    npos[1] = (*position)[nbpindex+1] ;
                    npos[2] = (*position)[nbpindex+2] ;
                    
                    // Find out th relative vector
                    for (u_int i =0 ; i < 3; ++i) {
                        relvec[i] = pos[i] - npos[i] ;
                    }
                    if (norm(relvec) < rcutoff_){
                        leonardJones(relvec, forcevector) ;
                        (*forcenew)[pvind]   += forcevector[0] ;
                        (*forcenew)[pvind+1] += forcevector[1] ;
                        (*forcenew)[pvind+2] += forcevector[2] ;
                    }
                }
            }
        }
    }
    
}

void ComputeKernel::sweepBoundaryCell(){
    
	// Temporary Positions
    std::vector<real_t> pos(3,0.0) ;
    
    // Relative vector
    std::vector<real_t> relvec(3,0.0) ;
    
    // Neighbouring particle position
    std::vector<real_t> npos(3,0.0) ;
    
    // Temporary vector for force vector
    std::vector<real_t> forcevector(3,0.0) ;
    
    // Stable distance
    real_t stbldist = 0.5 * sigma * std::pow(2, 1.0/6) ;
    
    // find the number of cellsize in one plane , assume cellssize in all direction are same
    u_int cellinplanes = cellx_ * celly_ ;
    
    for (u_int plane =0; plane < 6; ++plane) {
        for (u_int iter = plane * cellinplanes  ; iter < cellinplanes * (plane+1) ; ++iter) {
            
            // Find the cell
            u_int cell = (*boundaryCells)[iter] ;
            
            //Traverse the linked list in this cell
            for (int p_id = (*celllist)[cell]; p_id!= -1; p_id = (*particlelist)[p_id]  ){
                
                // Particle index
            	u_int pindex = p_id * 3 ;

            	// Find out the particle position
                pos[0] = (*position)[pindex] ;
                pos[1] = (*position)[pindex + 1] ;
                pos[2] = (*position)[pindex + 2] ;
                
                // Foe the neighbouring particle , give the same particle , as we want the reflection
                for (u_int i =0; i < 3; ++i) {
                    npos[i] = pos[i] ;
                }
                
                // Check the distance between the particle and the corresponding boundary
                int cord = plane / 2 ;
                real_t dist =  std::fabs(pos[cord] - dom_cords[plane]) ;
                

                if (dist < stbldist) {
                    u_int whichdir = plane%2 ;
                    if (whichdir ==0) {
                        npos[cord] = pos[cord] - (2* dist) ;
                    }
                    else npos[cord] = pos[cord] + (2 * dist) ;
                    
                    // Find out the relative vector
                    for (u_int i =0; i < 3; ++i) {
                        relvec[i] = pos[i] - npos[i] ;
                    }
                    leonardJones(relvec, forcevector) ;
                    (*forcenew)[pindex]   += forcevector[0] ;
                    (*forcenew)[pindex +1] += forcevector[1] ;
                    (*forcenew)[pindex +2] += forcevector[2] ;
                    
                }
            }
        }
    }
}

void ComputeKernel::applyGravity(){
    
    for (u_int p =0; p < numparticle_; ++p) {
        
        //Calculate the index of particle
        u_int pindex  = p * 3 ;
        
        //(*forcenew)[pindex] += 10 * (*mass)[p] ;
        //(*forcenew)[pindex+1] += -10 * (*mass)[p] ;
        (*forcenew)[pindex+2] += -10.0 * (*mass)[p] ;
        
    }
}

void ComputeKernel::copyForces(){

	for (u_int i =0 ; i < (*forcenew).size();++i ){

		(*forceold)[i] =  (*forcenew)[i] ;

	}
}

void ComputeKernel::positionUpdate(){

    
    // Update position for every particle
    for (u_int p =0; p < numparticle_; ++p) {
        
        // Find out the index of particle in particle buffer
        u_int pindex = p * 3 ;
        
        (*position)[pindex] += (timestep * (*velocity)[pindex]) + ( ((*forcenew)[pindex] * sqr(timestep))  / (2.0 * (*mass)[p] )) ;
        (*position)[pindex+1] += (timestep * (*velocity)[pindex+1]) + ( ((*forcenew)[pindex+1] * sqr(timestep))  / (2.0 * (*mass)[p] )) ;
        (*position)[pindex+2] += (timestep * (*velocity)[pindex+2]) + ( ((*forcenew)[pindex+2] * sqr(timestep))  / (2.0 * (*mass)[p] )) ;
        
        // Checking if particle left the domain , and if left the domain ....correct the position
        
        if ((*position)[pindex] < dom_cords[0] ) (*position)[pindex] = fmod((*position)[pindex], dom_len[0])+ dom_len[0];
        if ((*position)[pindex] > dom_cords[1] ) (*position)[pindex] = fmod((*position)[pindex], dom_len[0]) ;
        
        if ((*position)[pindex+1] < dom_cords[2] ) (*position)[pindex+1] = fmod((*position)[pindex+1], dom_len[1]) + dom_len[1];
        if ((*position)[pindex+1] > dom_cords[3] ) (*position)[pindex+1] = fmod((*position)[pindex+1], dom_len[1]);
        
        if ((*position)[pindex+2] < dom_cords[4] ) (*position)[pindex+2] = fmod((*position)[pindex+2], dom_len[2]) + dom_len[2] ;
        if ((*position)[pindex+2] > dom_cords[5] ) (*position)[pindex+2] = fmod((*position)[pindex+2], dom_len[2]);

    }
}

void ComputeKernel::positionUpdateReflect(){
    
    // Update position for every particle
    for (u_int p =0; p < numparticle_; ++p) {
        
        // Find out the index of particle in particle buffer
        u_int pindex = p * 3 ;
        
        (*position)[pindex] += (timestep * (*velocity)[pindex]) + ( ((*forcenew)[pindex] * sqr(timestep))  / (2.0 * (*mass)[p] )) ;
        (*position)[pindex+1] += (timestep * (*velocity)[pindex+1]) + ( ((*forcenew)[pindex+1] * sqr(timestep))  / (2.0 * (*mass)[p] )) ;
        (*position)[pindex+2] += (timestep * (*velocity)[pindex+2]) + ( ((*forcenew)[pindex+2] * sqr(timestep))  / (2.0 * (*mass)[p] )) ;
    }
}

void ComputeKernel::velocityupdate(){

    
    for (u_int p =0; p < numparticle_; ++p) {
        
        // Find the particle index
        u_int pindex  = p *3 ;

        (*velocity)[pindex] +=  (((*forcenew)[pindex] + (*forceold)[pindex] ) * timestep) / (2.0 * (*mass)[p]) ;
        (*velocity)[pindex+1] +=  (((*forcenew)[pindex+1] + (*forceold)[pindex+1] ) * timestep) / (2.0 * (*mass)[p]) ;
        (*velocity)[pindex+2] +=  (((*forcenew)[pindex+2] + (*forceold)[pindex+2] ) * timestep) / (2.0 * (*mass)[p]) ;
    }
}

void ComputeKernel::leonardJones(const std::vector<real_t> &relvec, std::vector<real_t> &forcevec){
    
    real_t distmod = norm(relvec) ;
    real_t distsqr = sqr(distmod) ;
    real_t sigmac = sigma / distmod ;
    real_t epsilonc = (24.0 * epsilon )/ distsqr ;

    real_t constant = sigmac * sigmac * sigmac * sigmac * sigmac * sigmac ;

    real_t con = (( 2 * constant ) - 1.0) ;


    forcevec[0] = epsilonc * constant * con * relvec[0] ;
    forcevec[1] = epsilonc * constant * con * relvec[1] ;
    forcevec[2] = epsilonc * constant * con * relvec[2] ;

}

void ComputeKernel::minDist(std::vector<real_t> &relvec, const std::vector<real_t> &pos, const std::vector<real_t> &npos, const std::vector<real_t> dom_length) {
    
    real_t distold = norm(relvec) ;
    
    // Temporary relative vector
    std::vector<real_t> tempvec(3,0.0) ;
    
    // Iterating over all 27 possibilities to find out the minimum Distance
    for (real_t x = npos[0] - dom_len[0]; x <= npos[0] + dom_len[0]; x += dom_len[0]) {
        for (real_t y = npos[1] - dom_len[1]; y <= npos[1] + dom_len[1]; y+= dom_len[1]) {
            for (real_t z = npos[2] - dom_len[2]; z<= npos[2] + dom_len[2]; z+= dom_len[2]) {
                
                // Find out the temporary relative vector
                tempvec[0] = pos[0] - x ;
                tempvec[1] = pos[1] - y ;
                tempvec[2] = pos[2] - z ;
                
                real_t dist = norm(tempvec) ;
                
                if(dist < distold){
                    relvec[0] = pos[0] - x ;
                    relvec[1] = pos[1] - y ;
                    relvec[2] = pos[2] - z ;
                    distold = dist ;
                }
            }
        }
    }
}

const real_t ComputeKernel::norm(const std::vector<real_t> vec) {
    
    double norm = 0.0 ;
    for (u_int i =0; i < vec.size(); ++i) {
        norm += vec[i] * vec[i] ;
    }
    return std::sqrt(norm) ;
}

// Initialise the cell and Particle list
void ComputeKernel::initialiselist(){

    // Initialising the cell list with -1
    for (u_int i =0; i < (*celllist).size(); ++i) {
        (*celllist)[i] = -1 ;
    }

    // Initialising the particle list
    for (u_int i =0 ; i < (*particlelist).size(); ++i) {
        (*particlelist)[i] = i ;
    }
}

// Update the link list
void ComputeKernel::updateLinkedList(){

    for (u_int particle =0; particle < numparticle_; ++particle) {

        // Find the position of this current particle
        u_int pos = particle * 3 ;

        // Find the cordinates of this particle
        real_t coordx = (*position)[pos] ;
        real_t coordy = (*position)[pos+1] ;
        real_t coordz = (*position)[pos+2] ;

        // Find the 3D cell index it lies in
        u_int xindex = (coordx - dom_cords[0]) / celllengthx ;
        u_int yindex = (coordy - dom_cords[2]) / celllengthy ;
        u_int zindex = (coordz - dom_cords[4]) / celllengthz ;

        // Find the global cell index
        u_int cellindex = globalCellIndex(xindex, yindex, zindex) ;
        // Go this cell in cell list and replace value with this particle index
        std::swap((*celllist)[cellindex], (*particlelist)[particle]) ;

    }
}



