//
//  ComputeKernel.hpp
//  Molecular-Dynamics
//
//  Created by Sagar Dolas on 13/06/16.
//  Copyright �� 2016 Sagar Dolas. All rights reserved.
//

#ifndef ComputeKernel_hpp
#define ComputeKernel_hpp

#include <stdio.h>
#include <cmath>
#include <math.h>
#include "ParticleData.hpp"
#include "DomainDecompose.hpp"

class ComputeKernel : public DomainDecompose {
    
private:
    
    const ParticleData<real_t> * const mass ;
    ParticleData<real_t> * const velocity ;
    ParticleData<real_t> * const position ;
    ParticleData<real_t> * const forcenew ;
    ParticleData<real_t> * const forceold ;
    ParticleData<int>    * const celllist ;
    ParticleData<int>    * const particlelist ;
    const ParticleData<u_int>  * const boundaryCells ;
    
    real_t sigma ;
    real_t epsilon ;
    real_t timestep ;
    
    std::vector<real_t> dom_len ;
    std::vector<real_t> dom_cords ; 
    
public:
    
    ComputeKernel(const ParticleData<real_t> &mass,ParticleData<real_t> &vel,ParticleData<real_t> &position, ParticleData<real_t> & forcenew, ParticleData<real_t> &forceold,ParticleData<int> & celllist, ParticleData<int> & particlelist,const ParticleData<u_int> &bdCell, const real_t xmin, const real_t xmax, const real_t ymin, const real_t ymax, const real_t zmin ,const real_t zmax , const real_t rcutoff, const u_int numparticles_,const real_t sigma,const real_t epsilon,const real_t timestep) ;
    
    ~ComputeKernel() ;
    
    // Force calculation on each particle
    void forceUpdate() ;
    
    // Force calculation for reflective boundary conditions
    void forceUpdateReflective() ;
    
    // Sweep the domain for applying the gravitational forces
    void applyGravity() ; // TODO 
    
    // Force Sweep for particles near the boundary
    void sweepBoundaryCell() ; 

    // Position Update keeping in mind periodic boundary conditions
    void positionUpdate() ;
    
    // Position Update keeping in mind reflective boudary condition
    void positionUpdateReflect() ;

    // Copy the forces
    void copyForces() ;

    // Velocity Update
    void velocityupdate() ;

    // norm Calculations
    const real_t norm(const std::vector<real_t> vec) ;

    // Leonard Jones Potential
    void leonardJones(const std::vector<real_t> &relvec,std::vector<real_t> &forcevec) ;

    // Minimum Distance
    void minDist(std::vector<real_t> &relvec,const std::vector<real_t> &pos, const std::vector<real_t> &npos,const std::vector<real_t> dom_length ) ;
    
    // Link list updation and computation
    void initialiselist() ;
    
    // Creating the linked list
    void updateLinkedList() ;

};

#endif /* ComputeKernel_hpp */
