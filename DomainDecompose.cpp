//
//  DomainDecompose.cpp
//  Molecular-Dynamics
//
//  Created by Sagar Dolas on 14/06/16.
//  Copyright �� 2016 Sagar Dolas. All rights reserved.
//

#include "DomainDecompose.hpp"

DomainDecompose::DomainDecompose(const real_t xmin, const real_t xmax, const real_t ymin, const real_t ymax, const real_t zmin , const real_t zmax , const real_t rcutoff,const u_int _numparticles ) : rcutoff_(rcutoff),numparticle_(_numparticles) {
    
    cellx_ = std::floor((xmax - xmin) / rcutoff_ ) ;
    celly_ = std::floor((ymax - ymin) / rcutoff_ ) ;
    cellz_ = std::floor((zmax - zmin) / rcutoff_ ) ;
    
    celllengthx = (xmax - xmin) / cellx_ ;
    celllengthy = (ymax - ymin) / celly_ ;
    celllengthz = (zmax - zmin) / cellz_ ;
}
DomainDecompose::~DomainDecompose(){}

// Finding the number of cells
const u_int DomainDecompose::numberOfCells(){
    
    return cellx_ * celly_ * cellz_ ; 
}

const u_int DomainDecompose::numberOfBoundaryCells(){
    return 2 * (cellx_ * celly_ + celly_*cellz_ + cellz_ * cellx_) ;
}

// This function is being defined keepind in mind the periodic boundary conditions
void DomainDecompose::findNeighboursPeriodic(std::vector<u_int> &nl,const u_int xindex, const u_int yindex, const u_int zindex){
    
    u_int tempx  = 0 ;
    u_int tempy  = 0 ;
    u_int tempz  = 0 ;
    u_int count = 0 ;
    
    for (int x = -1 ; x <2; ++x) {
        for (int y = -1; y <2; ++y) {
            for (int z =-1; z <2 ; ++z) {
                if ( x!= 0 || y!=0 || z!=0 ){
                    
                    tempx = (xindex + x + cellx_)%cellx_ ;
                    tempy = (yindex + y + celly_)%celly_ ;
                    tempz = (zindex + z + cellz_)%cellz_ ;
                    
                    nl[count] = globalCellIndex(tempx, tempy, tempz) ;
                    ++count ;
                }
            }
        }
    }
}

// Find Neighbours for Reflective boundary conditions
void DomainDecompose::findNeighboursReflective(std::vector<u_int> &nl,const u_int xindex, const u_int yindex, const u_int zindex){

    int tempx = 0 ;
    int tempy = 0 ;
    int tempz = 0 ;
    
    for (int x = -1 ; x <2; ++x) {
        for (int y = -1; y <2; ++y) {
            for (int z =-1; z <2 ; ++z) {
                if ( x!= 0 || y!=0 || z!=0 ){
                    tempx = static_cast<int>(xindex) + x ;
                    tempy = static_cast<int>(yindex) + y ;
                    tempz = static_cast<int>(zindex) + z ;
                    // taking cells only for reflective boundary conditions
                    if ( !(((tempx <0) || (tempx>static_cast<int>(cellx_-1))  ) || ( ( tempy<0 ) || ( tempy>static_cast<int>(celly_-1)) ) || ( (tempz<0) || (tempz> static_cast<int>(cellz_-1) ) )) ){
                        nl.push_back(globalCellIndex(tempx, tempy, tempz)) ;
                    }
                }
            }
        }
    }
    
}

// Find the global cell index in linear array of cells
const u_int DomainDecompose::globalCellIndex(const u_int xindex, const u_int yindex, const u_int zindex) {
    
    return  xindex + yindex * cellx_ + (zindex * cellx_ * celly_) ;

}

// Find the boundary cell ,
void DomainDecompose::findBoundaryCell(ParticleData<u_int> &b_Cell){
    
    u_int iter = 0 ;
    
    // Go in X direction and find out the number of cells
    for (u_int xdir = 0; xdir< cellx_; xdir+= cellx_-1) {
        for (u_int ydir =0; ydir<celly_; ++ydir) {
            for (u_int zdir =0; zdir < cellz_; ++zdir) {
                b_Cell[iter] = globalCellIndex(xdir, ydir, zdir) ;
                ++iter ;
            }
        }
    }
    
    // Go in Y direction and find out the numbers of cells
    for (u_int ydir = 0; ydir < celly_; ydir+= celly_-1) {
        for (u_int zdir =0; zdir < cellz_ ; ++zdir) {
            for (u_int xdir =0; xdir < cellx_; ++xdir) {
                b_Cell[iter] = globalCellIndex(xdir, ydir, zdir) ;
                ++iter ;
            }
        }
    }
    
    // Go in Z direction and find out the numbers of cells
    for (u_int zdir =0 ; zdir < cellz_; zdir+= cellz_-1) {
        for (u_int xdir = 0; xdir < cellx_; ++xdir) {
            for (u_int ydir =0 ; ydir < celly_; ++ydir) {
                b_Cell[iter] = globalCellIndex(xdir, ydir, zdir) ;
                ++iter ;
            }
        }
    }
}





