//
//  DomainDecompose.hpp
//  Molecular-Dynamics
//
//  Created by Sagar Dolas on 14/06/16.
//  Copyright �� 2016 Sagar Dolas. All rights reserved.
//

#ifndef DomainDecompose_hpp
#define DomainDecompose_hpp

#include <stdio.h>
#include <vector>
#include "ParticleData.hpp"
#include "PhysicalQuantity.h"
#include "Type.hpp"
#include "utility"
#include "algorithm"

// Forward Declaration for Particle Data class 

class DomainDecompose {
    
    
private:
    
public:
    
    u_int cellx_ ; // Number of cells in x - direction
    u_int celly_ ; // Number of cells in y - direction
    u_int cellz_ ; // Number of cells in z - direction
    real_t rcutoff_ ; // length of cell- in each direction
    u_int numparticle_ ; // Number of particles in simulation
    
    real_t celllengthx ;
    real_t celllengthy ;
    real_t celllengthz ;

    DomainDecompose(const real_t xmin, const real_t xmax, const real_t ymin, const real_t ymax, const real_t zmin , const real_t zmax , const real_t rcutoff, const u_int numparticles_) ;
    ~DomainDecompose() ;
    
    const u_int numberOfCells()  ;
    const u_int numberOfBoundaryCells() ; 
    
    // Neighbourlist in case of periodic Boundary Conditions
    void findNeighboursPeriodic(std::vector<u_int> &nl,const u_int xindex, const u_int yindex, const u_int zindex) ;
    
    // NL in case of reflective boundary conditions
    void findNeighboursReflective(std::vector<u_int> &nl,const u_int xindex, const u_int yindex, const u_int zindex ) ;
    
    // Finding out the global cell index of each cell
    const u_int globalCellIndex(const u_int xindex, const u_int yindex , const u_int zindex ) ;
    
    // Finding the index of boundary cells .. in fashion of x-cells , y-cells , z-cells 
    void findBoundaryCell(ParticleData<u_int> &b_Cell) ; 

};

#endif /* DomainDecompose_hpp */
