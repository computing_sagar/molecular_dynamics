//
//  FileReader.cpp
//  Molecular-Dynamics
//
//  Created by Sagar Dolas on 13/06/16.
//  Copyright �� 2016 Sagar Dolas. All rights reserved.
//

#include "FileReader.hpp"

// Consructor
FileReader::FileReader() {
    
}

//Read parameter names, their values and store in in a string to string map
bool FileReader::readParameters(const std::string &filename ){
    
    std::string param_name;
    std::string param_value;
    std::ifstream inputfile;
    inputfile.open(filename);

    if(!inputfile.is_open()){
        std::cerr<<"Could not open "<<filename<<std::endl;
        return false ;
    }
    else {
    	while(!inputfile.eof()){
            inputfile>>param_name>>param_value;
            params[param_name] = param_value;
        }
        inputfile.close();
    }
    return true ;
}



//Read input configuration and return the number of particles
void FileReader::readInputConfiguration(const std::string &datafile){

    real_t mass,pos_x,pos_y,pos_z,vel_x,vel_y,vel_z;
    std::ifstream input_file;
    input_file.open(datafile);

    num_particles =0;

    if(input_file.is_open()){
    	while (!input_file.eof()) {
    		input_file>>mass>>pos_x>>pos_y>>pos_z>>vel_x>>vel_y>>vel_z;
    		// Last line is read twice in the input , therefore ..t has to be checked with good bit
    		if (!input_file.good()) {
    			break ;
    		}
    		this->mass.push_back(mass);
    		this->pos.push_back(pos_x);
    		this->pos.push_back(pos_y);
    		this->pos.push_back(pos_z);
    		this->vel.push_back(vel_x);
    		this->vel.push_back(vel_y);
    		this->vel.push_back(vel_z);
    		++num_particles;
    		}
    	input_file.close();
    }
}



void FileReader::fillBuffers(ParticleData<real_t> &mass,
                         ParticleData<real_t> &velocity,
                         ParticleData<real_t> &position) {

    for (u_int i =0 ; i < this->num_particles ; ++i ){
        u_int vidx  = i * 3 ;
        mass[i] = this->mass[i] ;
        position[vidx]    = this->pos[vidx] ;
        position[vidx +1] = this->pos[vidx+1];
        position[vidx +2] = this->pos[vidx+2];
        velocity[vidx]    = this->vel[vidx] ;
        velocity[vidx +1] = this->vel[vidx+1] ;
        velocity[vidx +2] = this->vel[vidx+2];
    }
}

void FileReader::clearMem(){
	
    mass.clear() ;
	pos.clear()  ;
	vel.clear()  ;
}
