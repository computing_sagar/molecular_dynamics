//
//  FileReader.hpp
//  Molecular-Dynamics
//
//  Created by Sagar Dolas on 13/06/16.
//  Copyright �� 2016 Sagar Dolas. All rights reserved.
//

#ifndef FileReader_hpp
#define FileReader_hpp

#include <stdio.h>

#include <string>
#include <map>
#include <iostream>
#include <vector>
#include <fstream>
#include "Type.hpp"
#include "ParticleData.hpp"

class FileReader{


public:

	u_int num_params;
    u_int num_particles;

    std::map<std::string,std::string> params;
    std::vector<real_t> mass;
    std::vector<real_t> pos;
    std::vector<real_t> vel;

    //Constructor
    FileReader() ;

    //Parse the parameters
    bool readParameters(const std::string &filename);

    // Interface according to Assignment
    inline bool isDefined(const std::string &key) const {

    	   // Find the corresponding key if there
    	    auto it = params.find(key) ;
    	    if (it != params.end()) {
    	        return true ;
    	    }
    	    return false ;
    }
    
    // Get Parameter function 
    template<typename T>
    inline void GetParameter(const std::string& key, T &value) const {
    	std::string temp ;

        if (isDefined(key)) {
             temp = params.at(key);
             std::stringstream convert(temp) ;
             convert >>value ;
        }
    }

    //Read input configuration
    void readInputConfiguration(const std::string &datafile);

    // Fill the cudaDeviceBuffers
    void fillBuffers(ParticleData<real_t> &mass, ParticleData<real_t> &velocity, ParticleData<real_t> &position ) ;

    //Clear the temporary memory
    void clearMem () ;

};

#endif /* FileReader_hpp */
