#Declaring the variables
CC=g++ 

#Declaring the flags
CFLAGS = -c -std=c++11 -O3 -DNDEBUG -Wall -pedantic
FLAGS  = -std=c++11 -O3 -DNEBUG -Wall -pedantic

#Declaring the ColSamm Folder 
lib    = Source/ 		

all 				:MD 

MD				:FileReader.o DomainDecompose.o ComputeKernel.o ParaviewOutput.o Molecular.o VelocityDist.o
				$(CC) $(FLAGS) FileReader.o DomainDecompose.o ComputeKernel.o ParaviewOutput.o VelocityDist.o Molecular.o -o mdsim

FileReader.o			:FileReader.cpp
				$(CC) $(CFLAGS) FileReader.cpp

DomainDecompose.o		:DomainDecompose.cpp
				$(CC) $(CFLAGS) DomainDecompose.cpp

ComputeKernel.o			:ComputeKernel.cpp
				$(CC) $(CFLAGS) ComputeKernel.cpp

ParaviewOutput.o		:ParaviewOutput.cpp
				$(CC) $(CFLAGS) ParaviewOutput.cpp

Molecular.o			:Molecular.cpp
				$(CC) $(CFLAGS) Molecular.cpp

VelocityDist.o			:VelocityDist.cpp
				$(CC) $(CFLAGS) VelocityDist.cpp

clean				:
				rm -rf *.o mdsim

		
