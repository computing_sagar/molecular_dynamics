//
//  main.cpp
//  Molecular-Dynamics
//
//  Created by Sagar Dolas on 13/06/16.
//  Copyright �� 2016 Sagar Dolas. All rights reserved.
//

#include <iostream>
#include <utility>
#include <algorithm>
#include <string>

#include "FileReader.hpp"
#include "ParticleData.hpp"
#include "ComputeKernel.hpp"
#include "DomainDecompose.hpp"
#include "Type.hpp"
#include "ParaviewOutput.hpp"
#include "VelocityDist.hpp"

int main(int argc, const char * argv[]) {
    

	// Reading the data from file
	FileReader input ;
	
    if (!input.readParameters(argv[1])){
        std::cout<<"Not Possible to run Simulation"<<std::endl ;
        exit(-1) ; 
    };
	
    input.readInputConfiguration(argv[2]) ;

	// Fetch the Simulation Parameters
	u_int numparticles = input.num_particles ;
	u_int vis_spacing  = std::stoi(input.params["vis_space"]) ;
	real_t t_start 	   = std::stoi(input.params["t_start"]) ;
	real_t t_end       = std::stod(input.params["t_end"]) ;
	real_t delta_t     = std::stod(input.params["delta_t"]) ;

	// Fetch the Simulation domain Parameters
	real_t x_min       = std::stod(input.params["x_min"]) ;
	real_t x_max       = std::stod(input.params["x_max"]) ;
	real_t y_min 	   = std::stod(input.params["y_min"]) ;
	real_t y_max       = std::stod(input.params["y_max"]) ;
	real_t z_min 	   = std::stod(input.params["z_min"]) ;
	real_t z_max 	   = std::stod(input.params["z_max"]) ;
	real_t r_cut       = std::stod(input.params["r_cut"]) ;

	// Fetch the Force Parameters
	real_t eps 		   = std::stod(input.params["epsilon"]) ;
	real_t sigma       = std::stod(input.params["sigma"]) ;
    
    // Fetch the boundary Conditions and if there is no boundary conditions , the default boundary condition is periodic .
    std::string boundary = "periodic" ;
    auto iter = input.params.find("boundary") ;
    if(iter != input.params.end()){
        boundary = input.params["boundary"] ;
    }
    
	// Declare the Data Structure ... we are using Structure of Arrays
	ParticleData<real_t> mass(numparticles,PhysicalQuantity::scalar ) ;
	ParticleData<real_t> position(numparticles,PhysicalQuantity::vector ) ;
	ParticleData<real_t> velocity(numparticles,PhysicalQuantity::vector ) ;
	ParticleData<real_t> forcenew(numparticles,PhysicalQuantity::vector ) ;
	ParticleData<real_t> forceold(numparticles,PhysicalQuantity::vector ) ;

	// Fill the above initial buffers
	input.fillBuffers(mass,velocity,position) ;
    
	// Declare the Domain Decomposition Parameters
	DomainDecompose  domain(x_min,x_max,y_min,y_max,z_min,z_max,r_cut,numparticles) ;
	u_int num_cells = domain.numberOfCells() ;
	ParticleData<int> cell_list(num_cells,PhysicalQuantity::normal ) ;
	ParticleData<int> particle_list(numparticles,PhysicalQuantity::normal ) ;
    u_int num_cells_b = domain.numberOfBoundaryCells() ;
    ParticleData<u_int> boundaryCell(num_cells_b,PhysicalQuantity::normal) ;
    domain.findBoundaryCell(boundaryCell) ;
    
    
	// Declare the VTK output object
	vtkOutput out(input.params["name"]) ;

	ComputeKernel  compute(mass,velocity,position,forcenew,forceold,cell_list,particle_list,boundaryCell,x_min,x_max,y_min,y_max,z_min,z_max,r_cut,numparticles,sigma,eps,delta_t);

	u_int loop_iter = 0 ;

    // Main Simulation loop for periodic boundary condition
    
    if (boundary == "periodic"){
        
        out.writeVtkOutput(mass,position,forcenew,velocity,numparticles) ;
        // Initialize the Linked List
		compute.initialiselist() ;

		// Create link list -
		compute.updateLinkedList() ;

		// Update the force
		compute.forceUpdate() ;

		// Start the loop for subsequent computation
		for (real_t time = t_start ; time <= t_end ; time += delta_t) {

			++loop_iter ;

			compute.positionUpdate() ;

			compute.copyForces() ;

			compute.initialiselist();

			compute.updateLinkedList();

			compute.forceUpdate();

			compute.velocityupdate();

			if ((loop_iter % vis_spacing)==0){
				out.writeVtkOutput(mass,position,forcenew,velocity,numparticles) ;
			}
			if ((loop_iter % 100)==0){
				std::cout<<"iteration number :="<<loop_iter<<std::endl ;
			}
		}
    }
    
    else if (boundary =="reflective") {
    
    	
        std::cout<<"Running Special case of Reflective Boundary Condition"<<std::endl;

        // initialise the particle velocities as maxwell boltzmann
        VelocityDist vel(velocity) ;
        out.writeVtkOutput(mass,position,forcenew,velocity,numparticles) ;

    	// Initialize the linked list
        compute.initialiselist() ;
        
        // Create the link list
        compute.updateLinkedList() ;
        
        // Update the force
        compute.forceUpdateReflective() ;
        
        // Add Gravity ;
        compute.applyGravity() ;
        
        // Apply reflective boundary conditions
        compute.sweepBoundaryCell() ;
        
        // Start the loop for subsequent computation
        {
            for (real_t time = t_start; time <= t_end; time+= delta_t) {
                
                ++loop_iter ;
                
                compute.positionUpdateReflect() ;
                
                compute.copyForces() ;
                
                // Initialize the linked list
                compute.initialiselist() ;
                
                // Create the link list
                compute.updateLinkedList() ;
                
                // Update the force
                compute.forceUpdateReflective() ;
                
                // Add Gravity ;
                compute.applyGravity() ;
                
                // Apply reflective boundary conditions
                compute.sweepBoundaryCell() ;
                
                compute.velocityupdate() ;

                if ((loop_iter % vis_spacing)==0){
                    out.writeVtkOutput(mass,position,forcenew,velocity,numparticles) ;
                }
            	if ((loop_iter % 100)==0){
            				std::cout<<"iteration number :="<<loop_iter<<std::endl ;
            	}
                
            }
        }
    }
    else std::cout<<"Not possible"<<std::endl ;
    return 0;
}
