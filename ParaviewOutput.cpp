//
//  ParaviewOutput.cpp
//  Molecular-Dynamics
//
//  Created by Sagar Dolas on 13/06/16.
//  Copyright �� 2016 Sagar Dolas. All rights reserved.
//

#include "ParaviewOutput.hpp"
#include <iomanip>

//Constructor
vtkOutput::vtkOutput(std::string file_name){
    this->file_name = file_name;
    file_number = 0;
}

//Output file writer
void vtkOutput::writeVtkOutput(const ParticleData<real_t> &mass, const ParticleData<real_t> &position, const ParticleData<real_t> &forceNew, const
                               ParticleData<real_t> &velocity, int num_particles){
    
    
    std::ofstream outfile;
    std::string number = std::to_string(file_number) ;
    std::string file = this->file_name+number+".vtk";
    this->file_number++;

    outfile.open("vtk/"+file);
    if(!outfile.is_open()){
        std::cerr<<"Could not write vtk output to file "<<file_name<<std::endl;
        exit(-1);
    }

    outfile<<"# vtk DataFile Version 3.0"<<std::endl;
    outfile<<"SiWiRVisFile"<<std::endl;
    outfile<<"ASCII"<<std::endl;

    //Output position as an unstructured grid
    outfile<<"DATASET UNSTRUCTURED_GRID"<<std::endl;
    outfile<<"POINTS "<<num_particles<<" DOUBLE"<<std::endl;
    for(int i=0;i<num_particles;i++){
        outfile<<std::setprecision(8)<<position[i*3]<<" "<<position[i*3+1]<<" "<<position[i*3+2]<<std::endl;
    }

    //Output mass
    outfile<<"POINT_DATA "<<num_particles<<std::endl;
    outfile<<"SCALARS mass double"<<std::endl;
    outfile<<"LOOKUP_TABLE default"<<std::endl;
    for(int i=0;i<num_particles;i++){
        outfile<<std::setprecision(8)<<mass[i]<<std::endl;
    }

    //Output force
    outfile<<"VECTORS force double"<<std::endl;
    for(int i=0;i<num_particles;i++){
        outfile<<std::setprecision(8)<<forceNew[i*3]<<" "<<forceNew[i*3+1]<<" "<<forceNew[i*3+2]<<std::endl;
    }

    //Output velocity
    outfile<<"VECTORS velocity double"<<std::endl;
    for(int i=0;i<num_particles;i++){
        outfile<<std::setprecision(8)<<velocity[i*3]<<" "<<velocity[i*3+1]<<" "<<velocity[i*3+2]<<std::endl;
    }
}
