//
//  ParaviewOutput.hpp
//  Molecular-Dynamics
//
//  Created by Sagar Dolas on 13/06/16.
//  Copyright �� 2016 Sagar Dolas. All rights reserved.
//

#ifndef ParaviewOutput_hpp
#define ParaviewOutput_hpp

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include "Type.hpp"
#include "ParticleData.hpp"

class vtkOutput{
private:
    std::string file_name;
    int file_number;

public:
    vtkOutput(std::string file_name);
    void writeVtkOutput(const ParticleData<real_t> &mass, const ParticleData<real_t> &position, const ParticleData<real_t> &forceNew, const ParticleData<real_t> &velocity, int num_particles);

};

#endif /* ParaviewOutput_hpp */
