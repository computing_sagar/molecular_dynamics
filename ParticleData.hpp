//
//  ParticleData.hpp
//  Molecular-Dynamics
//
//  Created by Sagar Dolas on 13/06/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#ifndef ParticleData_hpp
#define ParticleData_hpp

#include <stdio.h>
#include <vector>
#include <iostream>

#include "PhysicalQuantity.h"
#include "Type.hpp"

        
template<typename T>
class ParticleData {
    
private:
    
    PhysicalQuantity type_ ;
    u_int numdata_ ;
    std::vector<T> data_ ;
    
public:
    
    // Constructor
    explicit ParticleData(const T _value , const u_int _numdata, const PhysicalQuantity _type ) ;
    explicit ParticleData(const u_int _numdata,const PhysicalQuantity _type) ;
    explicit ParticleData(const PhysicalQuantity _type) ;
    explicit ParticleData(const ParticleData<T> &obj) ;
    
    // Copy Assignment
    ParticleData<T>& operator= (const ParticleData<T> &obj) ;
    
    // Access operator
    const T & operator[](const u_int index) const ;
    T & operator [] (const u_int index) ;
    
    // Get Functions
    const u_int size() const ; // Returns the size of vector
    const PhysicalQuantity type() const ;
    
    // pushback function
    void pushback(const T value_) ;
    
    // resize function
    void resize(const u_int num) ;
    void resize(const u_int num, const T value_) ;
    
};

template<typename T>
ParticleData<T>::ParticleData(const T _value, const u_int _numdata, const PhysicalQuantity _type) :type_(_type), numdata_(_numdata) {
    
    if(type_ == PhysicalQuantity::vector) {
        data_.resize(3 * numdata_, _value) ;
    }
    else
        data_.resize(numdata_, _value) ;
    
}

template<typename T>
ParticleData<T>::ParticleData(const u_int _numdata , const PhysicalQuantity _type) : type_(_type),numdata_(_numdata){
    
    if(type_ == PhysicalQuantity::vector) {
        data_.resize(3 * numdata_, static_cast<T>(0.0)) ;
    }
    else
        data_.resize(numdata_,  static_cast<T>(0.0)) ;
    
}

template<typename T>
ParticleData<T>::ParticleData(const PhysicalQuantity _type) : type_(_type) {
    

}

template<typename T>
ParticleData<T>::ParticleData(const ParticleData<T>& obj) {
    
    this->type_ = obj.type() ;
    this->resize(obj.size()) ;
    
    for (u_int i =0 ; i < this->size(); ++i) {
        this->data_[i] = obj[i] ;
    }
}

template<typename T>
ParticleData<T>& ParticleData<T>::operator=(const ParticleData<T> &obj){
    
    if ((this->type_ == obj.type()) && (this->size() == obj.size() ) ) {
       
        for (u_int i =0 ; i < this->size(); ++i) {
            this->data_[i] = obj[i] ;
        }
    }
    else{
        std::cout<<"Something wrong with Copy assignment operator"<<std::endl ;
        exit(-1) ;
    }
}

template<typename T>
const T & ParticleData<T>::operator[](const u_int index) const {
    
    return data_[index] ;
    
}

template<typename T>
T & ParticleData<T>::operator[](const u_int index) {
    
    return data_[index] ;
    
}

template<typename T>
const u_int ParticleData<T>::size() const {
    
    return data_.size() ;
}

template<typename T>
const PhysicalQuantity ParticleData<T>::type() const {
    
    return this->type_ ;
    
}

template<typename T>
void ParticleData<T>::pushback(const T value_) {
    
    data_.push_back(value_) ;
}

template<typename T>
void ParticleData<T>::resize(const u_int num){

    data_.resize(num,static_cast<T>(0.0)) ;
}

template<typename T>
void ParticleData<T>::resize(const u_int num, const T value_){
    
    data_.resize(num,value_) ; 
    
}

#endif /* ParticleData_hpp */
