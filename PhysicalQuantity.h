//
//  PhysicalQuantity.h
//  Molecular-Dynamics
//
//  Created by Sagar Dolas on 14/06/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#ifndef PhysicalQuantity_h
#define PhysicalQuantity_h

enum PhysicalQuantity {
    
    normal =1,
    scalar =1 ,
    vector = 3
    
};

enum boundary {
    
    periodic ,
    relective
    
};



#endif /* PhysicalQuantity_h */
