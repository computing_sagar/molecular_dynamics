#This is Molecular Dynamics Simulations Assignment 4 

#Please follow the steps to ensure corectness of the program 

Step 1 : To compile the program , type in <make> , Once the program has been compiled without any warnings or errors , so executable ./mdsim will be generated . 

Step 2 : Program has both reflective and periodic boundary handling functionality so , take parameter file and data file for program to run . 

Step 3 : Type in ./mdsim <parameter file> <data file> for program . 

Step 4 : For the periodic boundary condition , we have used non-dimesional Maxwell-Boltzann velocities in order to see the magnitudes in scale , otherwise the velocities are really very small to be observed . Please use the given fluid.par fluid.dat for running the reflective boundary case . Also a video is provided to see the fluid simulations. 

/********** Word of Caution ********/

Please dont enter illogical entries since this version of program cannot take care of it . 
