//
//  VelocityDist.cpp
//  Molecular-Dynamics
//
//  Created by Sagar Dolas on 26/06/16.
//  Copyright �� 2016 Sagar Dolas. All rights reserved.
//

#include "VelocityDist.hpp"


VelocityDist::VelocityDist(ParticleData<real_t> &vel ){
    
    u_int nump = vel.size() /3 ;
    
    std::vector<real_t> fac(3,0.0) ;
    
    for (u_int p =0 ; p < nump; ++p) {
        
        // Find the index of particle
        u_int pindex  = p * 3 ;
        fac = MaxwB(300) ;
        
        vel[pindex] = fac[0] ;
        vel[pindex+1] = fac[1] ;
        vel[pindex+2] = fac[2] ;
                
    }

}

VelocityDist::~VelocityDist(){
    
    
}

const std::vector<real_t> VelocityDist::MaxwB(const real_t fac){
    
    std::vector<real_t> temp(3,0.0) ;
    do {
        
        a1 = 2.0 * std::rand() / ((double)RAND_MAX + 1.0 ) - 1.0 ;
        a2 = 2.0 * std::rand() / ((double)RAND_MAX + 1.0 ) - 1.0 ;
        a3 = 2.0 * std::rand() / ((double)RAND_MAX + 1.0 ) - 1.0 ;
        
        r = (a1 * a1) + (a2 * a2) + (a3 * a3) ;
        
    } while (r >= 1.0);
    
    s = std::sqrt(-2.0 * log(r)/r) ;
    
    temp[0] = a1 *s ;
    temp[1] = a2 *s ;
    temp[2] = a3 *s ;
    
    return temp ; 
    
}



