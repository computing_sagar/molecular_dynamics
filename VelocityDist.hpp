//
//  VelocityDist.hpp
//  Molecular-Dynamics
//
//  Created by Sagar Dolas on 26/06/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#ifndef VelocityDist_hpp
#define VelocityDist_hpp

#include <stdio.h>
#include <vector>
#include <random>

#include "ParticleData.hpp"
#include "Type.hpp"


class VelocityDist {
    
private:
    
    real_t a1 ;
    real_t a2 ;
    real_t a3 ;
    real_t s  ;
    real_t r  ;
    
    constexpr static const real_t k_b = 1.38066e-23 ;
    
public:
    
    VelocityDist(ParticleData<real_t> &vel) ;
    ~VelocityDist() ;
    
    const std::vector<real_t> MaxwB(const real_t fac) ;
    
};

#endif /* VelocityDist_hpp */
